import java.io.File;
import java.io.IOException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 正文参数 长2800，宽2000，压缩比0.5
		float compressRatio = 0.4f;
		int destWidth = 2000;
		int destHeight = 2800;
		
		String cutSrc = "E:\\test\\src";
		String custDest = "E:\\test\\temp";
		String compressSrc = custDest;
		String compressDest = "E:\\test\\dest";
		
		int nameStartIndex = 0;
		File file = new File(cutSrc);
		String[] files = file.list();
		for (String fileName : files) {
			String cutSrcFileName = cutSrc + "\\" + fileName;
			String cutDestFileName = custDest + "\\" + fileName;
			ImageCut cut = new ImageCut(cutSrcFileName, cutDestFileName, 0, 0, destWidth,
					destHeight);
			cut.startCut();

			ImgCompress compress;
			try {
				String compressSrcFileName = compressSrc + "\\" + fileName;
				String compressDestFileName = compressDest + "\\" + nameStartIndex++;
				compress = new ImgCompress(compressSrcFileName, compressDestFileName);
				compress.autoResize(compressRatio);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
